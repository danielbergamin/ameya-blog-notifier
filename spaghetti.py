# Import standard depedencies available in the aws lambda runtime env
import json
import urllib
import boto3
import gzip
import time
import math
from datetime import datetime, date, time, timedelta

# Import non-standard dependencies from our .pip/ requirements.txt install
# Note that you must first run: pip install -r requirements.txt -t .pip
# Then you can sls deploy
import os
import sys
here = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(here, "./.pip"))
import requests

print('Loading function dependencies')

#======================================================================================================================
# Lambda Entry Point
#======================================================================================================================
def lambda_handler(event, context):
    print '[lambda_handler] Start'

    try:
        # Get the blogging schedule
        blogging_schedule = define_ameyas_blogging_schedule()

        # Count current amount of posts on the site
        current_post_count = count_posts()

        # Now check if we're on track -- quick example:
        # 1 post will yield BLOGGING_SCHEDULE[1], which will evaluate to 1 week after the start date.
        # If there's still only 1 post over a week after the start, the condition will trigger.
        # Also double check if we are running on schedule (00:01 CT == 05:01 UTC)
        if blogging_schedule[current_post_count] < date.today() and time(05,00) <= datetime.now().time():
            release_the_hounds()
        else:
            print('[lambda_handler] All is well')

    # The 1 hour project classic
    except Exception as e:
        raise e
    print '[lambda_handler] End'

# Create an array containing all the dates for blog publishing
def define_ameyas_blogging_schedule():
    blog_dates = []
    start = date(2017, 9, 11)
    # Create an array of blog dates spanning a year
    for x in xrange(51):
        days_to_add = 7 * x
        blog_dates.append(start + timedelta(days=days_to_add))
    return blog_dates

 # Count the total number of blogs on the site with a dodgy scraper
def count_posts():
    blog_root = os.environ['BLOG_ROOT']
    response = requests.get(blog_root)
    # We earn our spaghetti title
    return response.text.count("BlogList-item ")

 # Page our SNS topic, which we've subscribed our good friend
def release_the_hounds():
    client = boto3.client('sns')
    response = client.publish(
        TopicArn=os.environ['SNS_TOPIC_ARN'],
        Message='Time to send Dan 50 bucks'
    )
    # Print the response to the logs for debugging purposes
    print response


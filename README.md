## Deploy instructions
In order to get this going, you'll need to do a few things first:

1. `pip install -r requirements.txt -t .pip`

2. Create your SNS topic defined in serverless.yml manually. I've used an existing one called 'alerts' as a default. You could also adapt serverless.yml to create a fresh topic automatically - https://serverless.com/framework/docs/providers/aws/events/sns/

3. Ensure you have the [Serverless|serverless.com] framework installed and your AWS credentials configured via `aws config`.

Now you can `sls deploy` away!

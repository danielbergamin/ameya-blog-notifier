Run `pip install -r requirements.txt -t .pip` from the project root to install dependencies under this directory.

Then, in your lambda_handler, have the following code before you import any non-standard modules that have been installed by pip:

```import os
import sys
here = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(here, "./.pip"))

import requests
...```
